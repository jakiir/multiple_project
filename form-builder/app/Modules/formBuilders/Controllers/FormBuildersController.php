<?php
namespace App\Modules\FormBuilders\Controllers;

use App\Http\Requests\ReportsRequest;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Modules\Reports\Models\Reports;
use App\Modules\Reports\Models\ReportsMapping;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\UserTypes;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Libraries\Encryption;
use Illuminate\Support\Facades\DB;
use App\Modules\Reports\Models\ReportHelperModel;
use App\Modules\Reports\Models\HelperModel;

class FormBuildersController extends Controller {

    public function __construct(){
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));

    }

    public function index()
    {
        return view("formBuilders::admin_templete",compact('report_data','usersList','selected_user'));
    }
}
